CONFIG = {
    "private": {
        "help": "Value that should not be included in a versions report",
        "default": "super secret",
        # Adding this will mask the value of this item in a versions report
        "sensitive": True,
    },
}
DYNE = {"vrs": ["vrs"]}
