import sys
import unittest.mock as mock

import pop.hub


def test_help():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="config")

    hub.config.integrate.load("pop_config", "pop_config")

    with mock.patch(
        "shutil.get_terminal_size", return_value=mock.MagicMock(columns=80)
    ):
        ret = hub.args.parser.help()

    assert "Logging Options" in ret
    assert "usage:" in ret
    if sys.version_info >= (3, 10):
        assert "options" in ret
    else:
        assert "optional arguments" in ret


def test_base_help_subcommand():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="config")

    hub.config.integrate.load("s1", "s1")

    with mock.patch(
        "shutil.get_terminal_size", return_value=mock.MagicMock(columns=80)
    ):
        ret = hub.args.parser.help()

    assert "Logging Options" in ret
    assert "usage:" in ret
    assert "positional arguments" in ret
    if sys.version_info >= (3, 10):
        assert "options" in ret
    else:
        assert "optional arguments" in ret


def test_help_subcommand():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="config")

    with mock.patch("sys.argv", ["pop-config", "test"]):
        hub.config.integrate.load("s1", "s1")

    with mock.patch(
        "shutil.get_terminal_size", return_value=mock.MagicMock(columns=80)
    ):
        ret = hub.args.parser.help()

    assert "Logging Options" in ret
    assert "usage:" in ret

    if sys.version_info >= (3, 10):
        assert "options" in ret
    else:
        assert "optional arguments" in ret
