import json
import os
import pathlib
import subprocess
import sys

import dict_tools.data as data
import pytest


def run_cli(*args, load, env: dict = None, check: bool = True):
    if env is None:
        env = {}

    if os.name == "nt" and "SYSTEMROOT" not in env:
        env["SYSTEMROOT"] = os.getenv("SYSTEMROOT")

    # Patch the python path to have the testing dyne directory
    env["PYTHONPATH"] = str(pathlib.Path(__file__).parent.parent.parent / "pypath")
    env["TEST_LOAD_STR"] = json.dumps(load)

    runpy = pathlib.Path(__file__).parent / "run.py"
    command = [sys.executable, str(runpy), *args]

    proc = subprocess.Popen(
        command,
        encoding="utf-8",
        env=env,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )
    if check:
        assert proc.wait() == 0, proc.stderr.read()
        # We only care about what was printed to the logs
        return data.NamespaceDict(json.load(proc.stdout))
    else:
        return proc.stdout.read() or proc.stderr.read()


@pytest.fixture(scope="session")
def cpath_dir():
    cpath = pathlib.Path(__file__).parent.parent.parent / "config"
    assert cpath.exists()
    yield cpath


@pytest.fixture(name="cli", scope="function")
def cli():
    return run_cli
