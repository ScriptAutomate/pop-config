def test_ordering_1(cli, cpath_dir):
    OPT = cli(
        "--config",
        str(cpath_dir / "full.conf"),
        "--alls",
        "cli",
        "--oscli",
        "cli",
        "--exdyned1",
        "EXDYNED1",
        "--exdyned2",
        "EXDYNED2",
        "--exdyned3",
        "EXDYNED3",
        "--exdyned4",
        "EXDYNED4",
        load=(["full", "d1", "m1", "m2"], "full", ["d1"]),
        env={"OS_ALONE": "os", "OS_CLI": "os", "ALLS": "os", "OS_BASE_SOURCE": "os"},
    )
    assert OPT.d1.exdyned1 == "EXDYNED1"
    assert OPT.d1.exdyned2 == "EXDYNED2"
    assert OPT.d1.exdyned3 == "EXDYNED3"
    assert OPT.d1.exdyned4 == "EXDYNED4"
    assert OPT.m2.base == "os"
    assert OPT.full.alls == "cli"
    assert OPT.full.oscli == "cli"
    assert OPT.full.osalone == "os"
