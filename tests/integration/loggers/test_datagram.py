import socket

import pytest


@pytest.fixture(name="sock")
def datagram_socket():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(("localhost", 9000))
    sock.settimeout(1)
    yield sock
    sock.close()


def test_datagram_dgram(cli, sock):
    stderr = cli(
        "--log-plugin=datagram",
        "--log-level=trace",
        f"--log-file=localhost:9000",
    )

    # For now it's good enough that it printed to stderr and didn't raise an error
    assert "[TRACE   ] trace\n" in stderr
