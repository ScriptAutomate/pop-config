import os
import socket

import pytest


@pytest.fixture(name="sock")
def socket_():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(("localhost", 9000))
    sock.settimeout(5)
    sock.listen(1)
    conn, addr = sock.accept()
    # Yield a client connection
    yield conn
    sock.close()


@pytest.fixture(name="unix")
def unix_socket(logfile):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(str(logfile))
    yield sock
    sock.close()


def test_socket(cli):
    stderr = cli(
        "--log-plugin=socket",
        "--log-level=trace",
        f"--log-file=localhost:9000",
    )

    # For now it's good enough that it printed to stderr and didn't raise an error
    assert "[TRACE   ] trace\n" in stderr


@pytest.mark.skipif(
    os.name == "nt", reason="This test does not run properly on windows"
)
def test_datagram_unix(logfile, cli):
    stderr = cli(
        "--log-plugin=datagram",
        "--log-level=trace",
        f"--log-file={logfile}",
    )

    # For now it's good enough that it printed to stderr and didn't raise an error
    assert "[TRACE   ] trace\n" in stderr
