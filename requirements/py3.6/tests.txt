#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.6/tests.txt requirements/tests.in
#
aiofiles==0.8.0
    # via
    #   -r requirements/extra/async.txt
    #   dict-toolbox
aiologger==0.6.1
    # via -r requirements/extra/async.txt
asynctest==0.13.0
    # via
    #   -r requirements/tests.in
    #   pytest-pop
attrs==22.1.0
    # via pytest
colorama==0.4.5
    # via rend
contextvars==2.4
    # via sniffio
dict-toolbox==2.3.1
    # via
    #   pop
    #   pop-config
    #   pytest-pop
    #   rend
flaky==3.7.0
    # via -r requirements/tests.in
immutables==0.19
    # via contextvars
importlib-metadata==4.8.3
    # via
    #   pluggy
    #   pytest
iniconfig==1.1.1
    # via pytest
jinja2==3.0.3
    # via rend
lazy-object-proxy==1.7.1
    # via pop
markupsafe==2.0.1
    # via jinja2
mock==4.0.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.4
    # via dict-toolbox
nest-asyncio==1.5.6
    # via
    #   pop-loop
    #   pytest-pop
packaging==21.3
    # via pytest
pluggy==1.0.0
    # via pytest
file:.#egg=pop-config
    # via
    #   -r requirements/tests.in
    #   pop
    #   pytest-pop
pop-loop==1.0.5
    # via pop
pop==22.0.1
    # via
    #   pop-config
    #   pop-loop
    #   pytest-pop
    #   rend
py==1.11.0
    # via pytest
pyparsing==3.0.7
    # via packaging
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.16.0
    # via
    #   -r requirements/tests.in
    #   pytest-pop
pytest-pop==9.0.0
    # via -r requirements/tests.in
pytest==7.0.1
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
pyyaml==6.0
    # via
    #   -r requirements/tests.in
    #   dict-toolbox
    #   pop
    #   rend
rend==6.5.0
    # via pop-config
sniffio==1.2.0
    # via pop-loop
toml==0.10.2
    # via
    #   -r requirements/tests.in
    #   rend
tomli==1.2.3
    # via pytest
typing-extensions==4.1.1
    # via
    #   immutables
    #   importlib-metadata
zipp==3.6.0
    # via importlib-metadata
