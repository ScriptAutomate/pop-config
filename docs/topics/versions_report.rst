Versions Report
===============

When filling out a bug report for a POP project, you can use the "--versions-report" flag to
create a report of your environment and config.

This makes it easy for developers to replicate your environment and the bug.

Simply run your pop app with "--versions-report" appended to the cli command you used to cause the issue.

I.E.

.. code-block:: bash

    $ python my_pop_project.py my_arg --versions-report

output:

.. code-block:: yaml

    argv: my_pop_project.py my_arg
    config:
      pop_config:
        log_datefmt: '%H:%M:%S'
        log_file: idem.log
        log_fmt_console: '[%(levelname)-8s] %(message)s'
        log_fmt_logfile: '%(asctime)s,%(msecs)03d [%(name)-17s][%(levelname)-8s] %(message)s'
        log_handler_options: *id001
        log_level: warning
        log_plugin: basic
      rend:
        pipe: yaml
    freeze:
    - pop==22.0.1
    - pop-config==10.2.1
    - pop-loop==1.0.5
    - rend==6.5.0
    locale:
    - en_US
    - UTF-8
    name: idem
    platform:
      machine: x86_64
      os: posix
      release: 6.0.2-2-MANJARO
      system: Linux
      version: '#1 SMP PREEMPT_DYNAMIC Sat Oct 15 13:31:58 UTC 2022'
    python: 3.10.7 (main, Sep  6 2022, 21:22:27) [GCC 12.2.0]
    version: 19.5.0


This is the information that is included in the versions report:

argv
----

This is the exact command on the CLI that was called, excluding the "--versions-report" flag

freeze
------

The output of ``pip freeze``


locale
------

The system locale information

name
----

The name of the POP app being used

version
-------

The version of the POP app being used

platform
--------

Basic information about the OS and architecture of the host machine.

python
------

The python version used to run the POP app.

config
------

This is the contents of everything loaded onto hub.OPT through pop-config.


Hiding sensitive values
=======================

If a config value contains sensitive information, such as the "acct_key" in idem,
Then mark it as "sensitive" in it's entry in the CONFIG dictionary of the project's conf.py.
This will mask the config option's value in version reports.

I.E.

.. code-block:: python

    # my_project/my_project/conf.py

    CONFIG = {
        "my_opt": {
            "default": None,
            "help": "My opt",
            # Marking this option as "sensitive" in the config will mask it's value in the versions report
            "sensitive": True,
        }
    }

.. code-block:: bash

    $ acct --versions-report

output:

.. code-block:: yaml

    argv: usr/bin/acct
    config:
      acct:
        acct_file: '********************************************************'
        acct_key: '********************************************'
