======================
Advanced Topics
======================

Root Rewriting with `root_dir`
=================================

If you define a `root_dir` config option in your main application, the root rewriting
system is enabled (but is not necessarily active) for your application and all its plugins.

The default value for `root_dir` should be "/".

Explicitly setting `root_dir=/` at run time guarantees that root rewriting will not be activated.

Activating Root Rewriting
-------------------------

Eligible config options are modified if `root_dir` is set either manually or automatically.

Setting `root_dir` via config file, environment variable or command line takes priority.

If `root_dir` is not set and the program is run as a non-root user, `root_dir` is automatically
set to ".{program_name}" in the user's home directory.

Eligible Config Options
-----------------------

Config options for your program (including its plugins) may be rewritten if the following are all true:

- The config option name ends in _dir, _path or _file
- The config option has a default that is an absolute path
- The config option has a default that includes its pop project name as a path component

Assuming your project is named "foo":

.. code-block:: python

    CONFIG = {
        "root_dir": {"default": "/"},
        "a_dir": {"default": "/var/log/foo"},
        "b_file": {"default": "/var/log/foo"},
        "c_path": {"default": "/var/log/foo/"},
        "w": {"default": "/var/log/foo"},  # doesn't end in _dir/_path/_file
        "x_dir": {"default": "/var/log/foobar"},  # foo isn't a path component
        "y_dir": {"default": "/var/log/"},  # project/plugin name not in path
        "z_dir": {"default": "path/to/foo"},  # not an absolute path
    }

Variables a-c may be rewritten, w-z will never be automatically rewritten by the root system.

When rewritten, the new root_dir is prepended to variables. If you set `root_dir` to myroot,
options will be set as if these were your defaults:

.. code-block:: python

    CONFIG = {
        "root_dir": {"default": "myroot"},
        "a_dir": {"default": "myroot/var/log/foo"},
        "b_file": {"default": "myroot/var/log/foo"},
        "c_path": {"default": "myroot/var/log/foo/"},
    }

A explicitly set option via config file, environment variable or command line will override
these rewritten values, if you set `root_dir=myroot` and `a_dir=/var/log/foo`, `a_dir`
will be preserved as if this were your config block:

.. code-block:: python

    CONFIG = {
        "root_dir": {"default": "myroot"},
        "a_dir": {"default": "/var/log/foo"},
        "b_file": {"default": "myroot/var/log/foo"},
        "c_path": {"default": "myroot/var/log/foo/"},
    }

Disabling Root Rewriting
------------------------

If you are a developer and do not want to enable the roots system, do not
provide a `root_dir` config option. If you want to use `root_dir` in your
project without root rewriting, consider naming it `root` or `root_path`.

If you are a user and do not want default paths to be rewritten, explicitly
set `root_dir` to "/", the default value.

Similarly, if you explicitly set any `_dir`, `_path` or `_file` config variable
to the default value, it will not be rewritten.
